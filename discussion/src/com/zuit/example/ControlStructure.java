package com.zuit.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main (String[] args){
        //[Section] Java Operators
        //Arithmetic: -, +, /, *, %
        //Comparison: <, >, >=, <=, ==, !=;
        //Logical: &&, ||, !;
        //Reassignment operator: =, +=, -=, *=, /=;
        //Increment and decrement operator: i++, ++i, --i, i--;

        //[Section] Selection Control Structure:
        //statements allows us to manipulate the flow of the code depending on the evaluation of the condition.
        //Syntax:
         /*
          if (condition){
            statement
          }
          else if (condition) {
            Statement
          }
          else{
            Statement
          }
          */

        int num1 = 36;
        if (num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5.");
        }
        else{
            System.out.println(num1 + " is not divisible by 5.");
        }

        //[SECTION] Short Circuiting - a technique applicable only to the AND & OR operators wherein if statements or other control exits early by ensuring safety operations or efficiency.
        //if(condition 1 || condition 2 || condition 3 || condition 4)

        //[Section] Ternary Operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
        System.out.println(result);

        //[Section] Switch Statement:
        //are used to control the flow structures that allow one code block out of many other code blocks;

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int directionValue = numberScanner.nextInt();

        switch(directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
