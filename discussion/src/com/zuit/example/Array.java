package com.zuit.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // [SECTION] Java Collection - are a single unit of objects. Useful for manipulating relevant pieces of data that can be used in different situation, commonly collection are used in loops.
    public static void main(String[] args){
        //[SECTION] ARRAY - in Java, arrays are container of values of the same type given a predefined amount/number of values.
        // Java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

        //Syntax for Array Declaration:
            //dataType[] identifier = new dataType[numOfElements]
            //"[]" indicates that the data type should be able to hold multiple values.
            //"new" used for non-primitive data types to tell Java to create the said variable
            //"numOfElements" will tell how many elements does our array can hold.
        //Array declaration:
        //if we are going to declare an array of int[], the default value of the elements will all be 0.
        //if array String, the default value of the elements will be null.
        int[] intArray = new int[5];
        //To initialize the value of our elements inside the array, we are going to use index.
        intArray[0] = 200;
        intArray[1] = 123;
        intArray[2] = 322;
        intArray[3] = 1200;
        intArray[4] = 1200;


        //this will just print out the memory address of the array.
        System.out.println(intArray);

        //to print out the intArray, we need to import the Array Class and use the .toString() method.
        //this method will convert the array as a string in the terminal.
        System.out.println(Arrays.toString(intArray));

        //Declaration and Initialization of an Array
        //Syntax: dataType[] identifier = {elementA, elementB, elementC ...}
                // the compiler automatically specifies the size by counting the number of elements during the initialization.

        String[] names = {"Seth", "Timothy", "Ayka"};
        System.out.println(Arrays.toString(names));

        //Sample Java array Methods:
        //Sort method:
        Arrays.sort(intArray);
        System.out.println("Order of items after sort: " + Arrays.toString(intArray));

        //Multidimensional Array(nested array)
            // A two-dimensional array, can be described by two lengths nested within each other, like a matrix.
                //first length is row, second length is the column, arrayName[number element of parent array][number of arrays in the child array]

        String[][] classroom = new String [3][3]; // it will become like this: [[],[],[]]
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        //This is only applicable to those two-dimensional
        System.out.println(Arrays.deepToString(classroom));

        //Note: In Java, the size of the array cannot be modified. If there is a need/add elements, you need to create a new array.

        //[SECTION] ArrayList - resizable arrays, wherein elements can be added or removed whenever it is needed.
        //Syntax:
            //ArrayList<T> identifier = new ArrayList<T>();
            //"<T>" is used to specify that the list can only have one type of object in a collection.
            //ArrayList cannot hold primitive data types, with the help of "Java wrapper classes" that provides a way to use this types as objects.
            //When we say object version of primitive data types with methods.

        //Declaration of an ArrayList
            // Example usage of primitive data type as a generic in the ArrayList
            //ArrayList<int> numbers = new ArrayList<int>();

            //Usage of integer.
            ArrayList<Integer> numbers = new ArrayList<Integer>();

            // Adding elements using add method
                //arrayName.add(element);
            numbers.add(1);

            System.out.println(numbers);
            //NOTE: Kapag Array List sya di na need gumamit ng .toString().
            // If you want to access the element
                //arrayListName.get(index);
                System.out.println(numbers.get(0));
            //Declaration with initialization: (just use arrays.asList method)
            ArrayList<String> students = new ArrayList<String>(Arrays.asList("Nicko", "Andrei"));
            System.out.println(students);
            // Add elements on the ArrayList students:
            students.add("John");
            System.out.println(students);

            //access the elements
            System.out.println(students.get(2));

            //Updating an element
            //arrayListName.set(index, updatedValue/element);
            students.set(2, "Juan");
            System.out.println(students);

            //remove a specific element
            //arrayListName.remove(index);
            students.remove(1);
            System.out.println(students);

            //Getting the arrayList size:
            //arrayListName.size();
            System.out.println(students.size());

            // Removing all the elements
            // arrayListName.clear();
            students.clear();
            System.out.println(students);
            System.out.println(students.size());

            // For more ArrayList Methods:
            // https://www.programiz.com/java-programming/library/arraylist

        // [SECTION] Hashmaps - most objects in Java are defined and are instantiated of classes that contains proper set of properties and methods. There might be use cases where it is not appropriate, or you may simply want a collection of data in key-value pairs.
        // 'keys' - referred as "fields" where in the values can be accessed through fields.
        //Syntax:
            //HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

        // Declaration of a Hashmap:
        HashMap<String, String> jobPosition = new HashMap<String, String>();

        //Methods in HashMap:

        //Add key-value pair
        //HashMapName.put(FieldName, fieldValue);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
        jobPosition.put("Developer", "Andrei");
        //Note: whenever the same key is used, it will be overwritten.
        jobPosition.put("Student", "Nicko");
        System.out.println(jobPosition);

        //Accessing element - we use field name because they are unique.
        //hashMapName.get("FieldName");
        System.out.println(jobPosition.get("Student")); // if the fieldName does not exist on the HashMap it will give us a null value.

        //Updating Values
        //HashMapName.replace("fieldNametoChange", "NewValue");
        jobPosition.replace("Student", "John Doe");
        System.out.println(jobPosition);

        //Remove an element
        //HashMapName.remove("key/fieldname");
         jobPosition.remove("Dreamer");
         System.out.println(jobPosition);

        //Retrieve HashMap keys
        //HashMap.keySet();
        System.out.println(jobPosition.keySet());

        //Retrieve the value from the HashMap
        //HashMapName.values();
        System.out.println(jobPosition.values());


        //Remove all the key-value pairs
        //HashMapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition);


        //DECLARATION OF HASHMAP WITH INITIALIZATION:
        HashMap<String, String> jobPosition2 = new HashMap<String, String>(){
            {
                put("Teacher", "John");
                put("Artist", "Jane");
                put("Comedian", "GB Labrador");
            }
        };

        System.out.println(jobPosition2);







    }
}
